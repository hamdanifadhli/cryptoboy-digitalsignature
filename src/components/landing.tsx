import { Button, Container, Grid, Typography } from "@material-ui/core";

export function Landing(props: any) {
  const { setpage } = props;
  return (
    <>
      <Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{
            backgroundColor: "#4a47a3",
            borderRadius: "10px",
            padding: "1rem",
            marginTop: "4rem",
          }}
        >
          <Grid item>
            <Typography
              variant="h5"
              style={{ textAlign: "center", color: "#FFF", marginTop: "4rem" }}
            >
              Welcome to Cryptoboy Digital Signature
            </Typography>
            <Typography
              variant="h6"
              style={{
                textAlign: "center",
                color: "#FFF",
                marginTop: "4rem",
                marginBottom: "4rem",
              }}
            >
              <Button
                variant="outlined"
                style={{
                  color: "white",
                  borderColor: "white",
                }}
                onClick={() => setpage(2)}
              >
                Click Here To Start
              </Button>
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
