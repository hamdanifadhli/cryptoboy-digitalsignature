import { Button, Container, Grid, Typography } from "@material-ui/core";

export function About() {
  return (
    <>
      <Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{
            backgroundColor: "#4a47a3",
            borderRadius: "10px",
            padding: "1rem",
            marginTop: "4rem",
          }}
        >
          <Grid item>
            <Typography
              variant="h5"
              style={{ textAlign: "center", color: "#FFF", marginTop: "4rem" }}
            >
              About Cryptoboy
            </Typography>
            <Typography
              variant="h6"
              style={{
                textAlign: "center",
                color: "#FFF",
                marginTop: "1rem",
                marginBottom: "3rem",
              }}
            >
              Cryptoboy merupakan project program kriptografi yang disusun oleh
              Hamdani Fadhli dan Gede Satya Adi Dharma, Mahasiswa Teknik Elektro
              2017, dalam pemenuhan tugas Mata Kuliah Kriptografi
            </Typography>
            <Grid container spacing={3} style={{ textAlign: "center" }}>
              <Grid item xs={6}>
                <img
                  src="./img/hamdani.jpg"
                  alt=""
                  style={{ width: "24rem", height: "24rem" }}
                />
                <Typography
                  variant="body1"
                  style={{
                    textAlign: "center",
                    color: "#FFF",
                    marginTop: "1rem",
                    marginBottom: "3rem",
                  }}
                >
                  Hamdani Fadhli 13217058
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <img
                  src="./img/satya.jpg"
                  alt=""
                  style={{ width: "24rem", height: "24rem" }}
                />
                <Typography
                  variant="body1"
                  style={{
                    textAlign: "center",
                    color: "#FFF",
                    marginTop: "1rem",
                    marginBottom: "3rem",
                  }}
                >
                  Gede Satya Adi Dharma 13217016
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
