import { Button, Container, Grid, Typography } from "@material-ui/core";
import { useState } from "react";
import { Bin2string } from "../helper/bin2string";
import { Keygen } from "../helper/crypt";
import { Decryption } from "../helper/decryption";
import { Encryption } from "../helper/encryption";
import { Has } from "../helper/hash";
import FileSaver from "file-saver";

export function MainT() {
  const [key, setkey] = useState({
    pk: [0, 0],
    sk: [0, 0],
  });

  const [file, setfile] = useState({
    name: "",
    type: "",
  });

  const [content, setcontent] = useState("");

  const [digest, setdigest] = useState({
    dig: "",
    view: "",
  });

  const [verif, setverif] = useState({
    done: false,
    verif: false,
  });

  const [include, setinclude] = useState(false);
  const [start, setstart] = useState(false);

  const handleFile = (e: any) => {
    const content = e.target.result;
    if (content.split(",")[0] === "sk") {
      setkey({
        ...key,
        sk: [Number(content.split(",")[1]), Number(content.split(",")[2])],
      });
    } else if (content.split(",")[0] === "pk") {
      setkey({
        ...key,
        pk: [Number(content.split(",")[1]), Number(content.split(",")[2])],
      });
    }
  };

  const handleFile2 = (e: any) => {
    const content = e.target.result;
    let msg = Has(content);
    let dig = "";
    let chip = Encryption(msg, key.pk);
    for (let index = 0; index < chip.length; index++) {
      let element = chip[index].toString(16);
      if (element.length < 4) {
        let tambah0 = 4 - element.length;
        for (let index = 0; index < tambah0; index++) {
          element = "0" + element;
        }
      }
      dig = dig + element;
    }
    let joy = dig.match(/.{1,4}/g);
    let newjoy = joy?.join(" ");
    if (newjoy === undefined) {
      newjoy = "";
    }
    setcontent(
      content +
        `
<ds>${dig}</ds>`
    );
    setdigest({
      dig: dig,
      view: newjoy,
    });
  };

  const handleChangeFile = (file: any) => {
    let fileData = new FileReader();
    fileData.onloadend = handleFile;
    fileData.readAsText(file);
  };

  const handleChangeFile2 = (file: any) => {
    setfile({
      name: file.name,
      type: file.type,
    });
    let fileData = new FileReader();
    fileData.onloadend = handleFile2;
    fileData.readAsText(file);
  };

  const handleFile3 = (e: any) => {
    let content = e.target.result;
    setstart(true);
    if (content.includes("<ds>") && content.includes("</ds>")) {
      let diestu = content.split("<ds>")[1].replace("</ds>", "");
      content = content.split("<ds>")[0];
      content = content.slice(0, content.length - 1);
      let joy = diestu.match(/.{1,4}/g);
      let newjoy = joy?.join(" ").split(" ");
      let chip: number[] = [];
      for (let index = 0; index < newjoy.length; index++) {
        let element = parseInt(newjoy[index], 16);
        chip.push(element);
      }
      let dec = Decryption(chip, key.sk);
      for (let index = 0; index < chip.length; index++) {
        let element = chip[index].toString(16);
        if (element.length < 4) {
          let tambah0 = 4 - element.length;
          for (let index = 0; index < tambah0; index++) {
            element = "0" + element;
          }
        }
      }
      let msg = Has(content);
      setinclude(true);
      if (Bin2string(dec) === msg) {
        setverif({
          done: true,
          verif: true,
        });
      } else {
        setverif({
          done: true,
          verif: false,
        });
      }
    } else {
      setinclude(false);
    }
  };

  const handleChangeFile3 = (file: any) => {
    let fileData = new FileReader();
    fileData.onloadend = handleFile3;
    fileData.readAsText(file);
  };

  return (
    <>
      <Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{
            backgroundColor: "#4a47a3",
            borderRadius: "10px",
            padding: "1rem",
            marginTop: "4rem",
          }}
        >
          <Grid item xs={12}>
            <Typography
              variant="h5"
              style={{ textAlign: "center", color: "#FFF" }}
            >
              Key Generation
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              style={{ textAlign: "center", color: "#FFF", marginTop: "1rem" }}
            >
              <Grid item xs={4}>
                pk :{" "}
                {"<0x" +
                  key.pk[0].toString(16) +
                  ", 0x" +
                  key.pk[1].toString(16) +
                  ">"}
              </Grid>
              <Grid item xs={4}>
                sk :{" "}
                {"<0x" +
                  key.sk[0].toString(16) +
                  ", 0x" +
                  key.sk[1].toString(16) +
                  ">"}
              </Grid>
              <Grid item xs={4}>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Button
                      variant="outlined"
                      style={{
                        color: "white",
                        borderColor: "white",
                      }}
                      onClick={() => {
                        let key = Keygen(6);
                        setkey({
                          pk: key.pk,
                          sk: key.sk,
                        });
                      }}
                      fullWidth
                    >
                      Generate
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      variant="outlined"
                      style={{
                        color: "white",
                        borderColor: "white",
                      }}
                      onClick={() => {
                        var blob = new Blob(
                          ["pk," + key.pk[0] + "," + key.pk[1]],
                          {
                            type: "text/plain;charset=utf-8",
                          }
                        );
                        FileSaver.saveAs(blob, "key.pub");
                        var blob2 = new Blob(
                          ["sk," + key.sk[0] + "," + key.sk[1]],
                          {
                            type: "text/plain;charset=utf-8",
                          }
                        );
                        FileSaver.saveAs(blob2, "key.pri");
                      }}
                      fullWidth
                    >
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                  style={{ marginTop: "1rem" }}
                >
                  <Grid item>
                    <form>
                      <Button
                        variant="outlined"
                        style={{
                          color: "white",
                          borderColor: "white",
                          marginRight: "1rem",
                        }}
                      >
                        <label
                          htmlFor="my-file-input"
                          style={{ marginRight: "1rem" }}
                        >
                          Load SK{" "}
                        </label>
                        <input
                          type="file"
                          accept=".pri"
                          onChange={(e) => {
                            if (e.target.files === null) {
                            } else {
                              handleChangeFile(e.target.files[0]);
                            }
                          }}
                        />
                      </Button>
                    </form>
                  </Grid>
                  <Grid item>
                    <form>
                      <Button
                        variant="outlined"
                        style={{
                          color: "white",
                          borderColor: "white",
                          marginRight: "1rem",
                        }}
                      >
                        <label
                          htmlFor="my-file-input"
                          style={{ marginRight: "1rem" }}
                        >
                          Load PK{" "}
                        </label>
                        <input
                          type="file"
                          accept=".pub"
                          onChange={(e) => {
                            if (e.target.files === null) {
                            } else {
                              handleChangeFile(e.target.files[0]);
                            }
                          }}
                        />
                      </Button>
                    </form>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          {key.pk[0] === 0 && key.sk[0] === 0 ? (
            <Grid item xs={12}>
              <Typography
                variant="body1"
                style={{
                  textAlign: "center",
                  color: "#FFF",
                  marginTop: "1rem",
                }}
              >
                Please Generate Key First
              </Typography>
            </Grid>
          ) : (
            <></>
          )}
          <Grid item xs={12}>
            <Typography
              variant="h5"
              style={{ textAlign: "center", color: "#FFF", marginTop: "1rem" }}
            >
              Signature
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <form>
              <Button
                variant="outlined"
                disabled={key.pk[0] === 0 && key.sk[0] === 0 ? true : false}
                style={{
                  color: "white",
                  borderColor: "white",
                  marginRight: "1rem",
                }}
              >
                <label htmlFor="my-file-input" style={{ marginRight: "1rem" }}>
                  Load File{" "}
                </label>
                <input
                  type="file"
                  accept="*"
                  disabled={key.pk[0] === 0 && key.sk[0] === 0 ? true : false}
                  onChange={(e) => {
                    if (e.target.files === null) {
                    } else {
                      handleChangeFile2(e.target.files[0]);
                    }
                  }}
                />
              </Button>
            </form>
          </Grid>
          <Grid item xs={8}>
            <Typography
              variant="body1"
              style={{
                textAlign: "center",
                color: "#FFF",
                padding: "1rem",
              }}
            >
              {key.pk[0] === 0 && key.sk[0] === 0
                ? "Generate Key Please"
                : digest.dig === ""
                ? "Load File First"
                : digest.view}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="outlined"
              disabled={content !== "" ? false : true}
              style={{
                color: "white",
                borderColor: "white",
                marginTop: "1rem",
              }}
              onClick={() => {
                var blob = new Blob([content], { type: file.type });
                FileSaver.saveAs(blob, file.name);
              }}
              fullWidth
            >
              Download Sign File
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="h5"
              style={{
                textAlign: "center",
                color: "#FFF",
                padding: "1rem",
              }}
            >
              Verify
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <form>
              <Button
                variant="outlined"
                disabled={key.pk[0] === 0 && key.sk[0] === 0 ? true : false}
                style={{
                  color: "white",
                  borderColor: "white",
                  marginRight: "1rem",
                }}
              >
                <label htmlFor="my-file-input" style={{ marginRight: "1rem" }}>
                  Load File{" "}
                </label>
                <input
                  type="file"
                  accept="*"
                  disabled={key.pk[0] === 0 && key.sk[0] === 0 ? true : false}
                  onChange={(e) => {
                    if (e.target.files === null) {
                    } else {
                      handleChangeFile3(e.target.files[0]);
                    }
                  }}
                />
              </Button>
            </form>
          </Grid>
          <Grid item xs={8}>
            <Typography
              variant="h6"
              style={{
                textAlign: "center",
                color: "#FFF",
                padding: "1rem",
              }}
            >
              {key.pk[0] === 0 && key.sk[0] === 0
                ? "Generate Key Please"
                : start
                ? include === false
                  ? "File Unsigned"
                  : verif.done === true
                  ? verif.verif === true
                    ? "Real File"
                    : "Fake File"
                  : "Input File"
                : "Load File First"}
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
