function findInverse(a: number, p: number) {
  let out = 0;
  for (let index = 0; index < p; index++) {
    if ((a * index) % p === 1) {
      out = index;
      break;
    }
  }
  return out;
}

function isPrime(n: number) {
  if (n <= 1) {
    return false;
  }
  if (n <= 3) {
    return true;
  }
  if (n % 2 === 0 || n % 3 === 0) {
    return false;
  }
  let i = 5;
  while (i * i <= n) {
    if (n % i === 0 || n % (i + 2) === 0) {
      return false;
    }
    i = i + 6;
  }
  return true;
}

export function Decrypt(c: number, sk: number[]) {
  let dec = BigInt(c) ** BigInt(sk[0]) % BigInt(sk[1]);
  return Number(dec);
}

export function Encrypt(m: number, pk: number[]) {
  let chip = BigInt(m) ** BigInt(pk[0]) % BigInt(pk[1]);
  return Number(chip);
}

export function Keygen(mxb: number) {
  let maxval = 2 ** mxb;
  let p,
    q,
    count = 0;
  let rando = Math.floor(Math.random() * 5) + 4;

  while (count < rando) {
    if (isPrime(maxval)) {
      if (count === 0) {
        p = maxval;
      } else if (count === rando - 1) {
        q = maxval;
        break;
      }
      count = count + 1;
    }
    maxval = maxval - 1;
  }

  let phi = (Number(p) - 1) * (Number(q) - 1);
  let n = Number(p) * Number(q);
  let e = Math.floor(n / Number(q)) * (Math.floor(Math.random() * 5) + 5);

  while (!isPrime(e)) {
    e = e + 1;
  }

  let d = findInverse(e, phi);
  let pk = [e, n];
  let sk = [d, n];
  return { pk, sk };
}
