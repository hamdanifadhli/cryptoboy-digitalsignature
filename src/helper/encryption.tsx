import { Encrypt } from "./crypt";

export function Encryption(msg: string, pk: number[]) {
  let bytes: number[] = [];
  let bytesenc: number[] = [];
  for (var i = 0; i < msg.length; ++i) {
    var code = msg.charCodeAt(i);
    bytes.push(code);
  }
  for (let index = 0; index < bytes.length; index++) {
    let element = bytes[index];
    bytesenc.push(Encrypt(element, pk));
  }
  return bytesenc;
}
