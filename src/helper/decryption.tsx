import { Decrypt } from "./crypt";

export function Decryption(chip: number[], sk: number[]) {
  let bytesdec: number[] = [];
  for (let index = 0; index < chip.length; index++) {
    let element = chip[index];
    bytesdec.push(Decrypt(element, sk));
  }
  return bytesdec;
}
