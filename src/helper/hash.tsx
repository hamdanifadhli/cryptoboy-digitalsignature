import sha1 from "sha1";

export function Has(data: any) {
  const crypt = sha1(data);
  return crypt;
}
