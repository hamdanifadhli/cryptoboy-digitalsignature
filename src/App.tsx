import React, { useState } from "react";
import { AppBar, Container, Toolbar, Typography } from "@material-ui/core";
import { MainT } from "./components/main";
import { Landing } from "./components/landing";
import { About } from "./components/about";

function App() {
  const [page, setpage] = useState(0);
  return (
    <>
      <AppBar
        position="static"
        elevation={0}
        style={{
          backgroundColor: "#413c69",
        }}
      >
        <Container>
          <Toolbar>
            <Typography
              variant="h6"
              style={{ flex: 1 }}
              onClick={() => {
                setpage(0);
              }}
            >
              Cryptoboy
            </Typography>
            <Typography variant="h6" style={{ flex: 1 }}>
              Digital Signature
            </Typography>
            <Typography
              variant="h6"
              onClick={() => {
                setpage(1);
              }}
            >
              About Us
            </Typography>
          </Toolbar>
        </Container>
      </AppBar>
      {page === 0 ? (
        <Landing setpage={setpage} />
      ) : page === 1 ? (
        <About />
      ) : page === 2 ? (
        <MainT />
      ) : (
        <></>
      )}
    </>
  );
}

export default App;
